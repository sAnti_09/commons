package commons;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public abstract class Savable {
	private String defaultName = "savable";
	private String extension;
	protected String separator = "::";

	public boolean save(String directory, String name) {
		File file = fileMaker(directory, name);
		PrintWriter out;

		try {
			out = new PrintWriter(file);
			out.print(toString());
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	public boolean load(String directory, String name) {
		File file = fileMaker(directory, name);
		Scanner scan = null;

		try {
			scan = new Scanner(file);
			while (scan.hasNext()) {
				String scanLine = scan.nextLine();
				if(!parse(scanLine, scan))
					return false;
			}
			scan.close();
		} catch (FileNotFoundException e) {
			System.err.println(file.getAbsolutePath() + " not found");
			return false;
		}
		
		return true;
	}
	
	private File fileMaker(String directory, String name){
		directory = checkForDefaultDirectory(directory);
		name = checkForDefaultName(name);
		return new File(directory + "/" + name + extension);
	}
	
	protected String checkForDefaultDirectory(String directory) {
		return directory == null ? new File("").getAbsolutePath() : directory;
	}
	
	protected String checkForDefaultName(String name) {
		return name == null? defaultName : name;
	}
	
	protected void setDefaults(String name, String extension){
		defaultName = name;
		this.extension = extension;
	}
	
	protected double toDouble(String s) {
		try {
			return Double.parseDouble(s.trim());
		} catch (Exception e) {
			System.err.println(s + " cannot be converted to integer");
			return 0;
		}
	}
	
	protected int toInt(String s) {
		try {
			return Integer.parseInt(s.trim());
		} catch (Exception e) {
			System.err.println(s + " cannot be converted to integer");
			return 0;
		}
	}
	
	@Override
	public abstract String toString();
	
	public abstract boolean parse(String scanLine, Scanner scan);
	
	

}
